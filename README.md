# Game of three

This project is a game of three challenge.

## Goal

The Goal is to implement a game with two independent units – the players –
communicating with each other using an API.

## Description

When a player starts, it incepts a random (whole) number and sends it to the second
player as an approach of starting the game.
The receiving player can now always choose between adding one of {­1, 0, 1} to get
to a number that is divisible by 3. Divide it by three. The resulting whole number is
then sent back to the original sender.
The same rules are applied until one player reaches the number 1(after the division).
See example below.

## Organization

We use Dependecy Injects to handle complex classes. Immutable JS with Ramda to provide a functional approach when it as needed.

### Internal Dependencies

There are no circle dependency, the project was done with the segregation in mind. all the modules could easily be
splited in few projects.

# How to test it

## Unit

There are several tests (unit) ones inside the **tests** folder.

You can run

> npm run test

## Demo

You can run it in a "automatic way"

First all run the server:

> npm run start

run the command:

> npm run game

after

> npm run game:listener

You will create two instances of the demo one able to challenge and other just a listener with some automatic decisions.

### Demo Folder

The "demo" folder is a simple implementation. I left it simple because it's a simple how to use.
I wanted to prepare a good frontend with react but I was not able to produce both in my scheduled time.
