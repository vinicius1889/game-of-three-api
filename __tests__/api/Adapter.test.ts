import { adaptMoveRequestToPlayerMove } from '../../src/api/Adapter';
import { MoveRequest } from '../../src/api/APITypes';
import { EnumMoveSelection } from '../../src/models';

describe('Adapter tests ', () => {
  it('should test the adapter of the move request to a PlayerMove values', () => {
    const moveRequest = {
      matchId: '_MATCH_ID_TEST_ADAPTER_123',
      playerId: '_PLAYER_ID',
      originalNumber: 3000,
      operation: 'ZERO',
    } as MoveRequest;

    const adapted = adaptMoveRequestToPlayerMove(moveRequest);

    expect(adapted.moveSelection).toBe(EnumMoveSelection.ZERO);
    expect(adapted.originalNumber).toBe(moveRequest.originalNumber);
    expect(adapted.player.id).toBe(moveRequest.playerId);
  });
});
