import { Map, List } from 'immutable';
import { EnumMatchStatus, Match } from '../../src/models';
import {
  notifyChallengedPlayer,
  notifyChallengeAnswer,
  notifyPlayersAfterMove,
} from '../../src/api/SocketSender';

describe('Socket Sender tests', () => {
  const match: Match = {
    id: 'ID_MATCH_SOCKET_TEST',
    challenger: { id: 'CH_123' },
    challenged: { id: 'CHD_456' },
    moves: List(),
    status: EnumMatchStatus.WAITING_PLAYER_ACCEPT,
  };

  it('it should notify the challenged player', () => {
    const socket = FakeSocket.instance();
    const brandNewMatch = { ...match };
    brandNewMatch.status = EnumMatchStatus.WAITING_PLAYER_ACCEPT;
    notifyChallengedPlayer(brandNewMatch, socket);
    const challengedObject = socket.events.get(`new_challenge`);
    const rooms = socket.destinyEvents.includes(match.challenged.id);
    expect(challengedObject).toBeDefined();
    expect(rooms).toBeTruthy();
  });

  it('it should notify the challenger player', () => {
    const socket = FakeSocket.instance();
    const acceptedChallenge = { ...match };
    acceptedChallenge.status = EnumMatchStatus.START;

    notifyChallengeAnswer(acceptedChallenge, socket);

    expect(socket.events.has('new_game')).toBeTruthy();
    expect(socket.destinyEvents.includes(match.challenger.id)).toBeTruthy();
  });

  it('it should notify the player after each move', () => {
    const socket = FakeSocket.instance();
    const currentMatch = { ...match };
    currentMatch.status = EnumMatchStatus.PLAYING;
    notifyPlayersAfterMove(currentMatch, socket);
    expect(socket.events.has('usr_game_move')).toBeTruthy();
  });

  it('it should notify the player that game has finished', () => {
    const socket = FakeSocket.instance();
    const currentMatch = { ...match };
    currentMatch.status = EnumMatchStatus.FINISHED;
    notifyPlayersAfterMove(currentMatch, socket);
    expect(socket.events.has('usr_game_move')).toBeTruthy();
  });
});

/**
 * Fake socket server
 *
 */
class FakeSocket {
  events: Map<string, any> = Map();
  destinyEvents: List<string> = List();
  rooms: List<string> = List();

  static instance(): FakeSocket {
    return new FakeSocket();
  }

  emit(event: string, obj: any): void {
    this.events = this.events.set(event, obj);
  }

  to(destiny: string): FakeSocket {
    this.destinyEvents = this.destinyEvents.push(destiny);
    return this;
  }
  join(room: string): void {
    this.rooms = this.rooms.push(room);
  }
}
