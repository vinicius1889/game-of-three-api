import {
  createSocketServer,
  getDataService,
  getPlayerChallengeService,
  getPlayerConnectionService,
  getPlayerMoveService,
} from '../../src/factory';
import { IDataService } from '../../src/services/dataService/IDataService';

describe('Factory tests - Socket Server', () => {
  it('It should create a new socket io server instance using the default items', () => {
    const ioServer = createSocketServer();
    expect(ioServer).toHaveProperty('sockets');
  });
  it('It should create a new socket io server with empty state', () => {
    const ioServer = createSocketServer();
    const {
      sockets: { rooms },
    } = ioServer;
    expect(rooms.length).toBe(0);
  });
});

describe('Factory tests - IDataService', () => {
  it('Should get an instance of DataService but it should be standalone.', () => {
    const dataService: IDataService = getDataService();
    const anotherDataService: IDataService = getDataService();
    expect(dataService).toEqual(anotherDataService);
  });
});

describe('Factory tests - PlayerChallengeService', () => {
  it('Should get player challenge service instance', () => {
    const playerChallengeService = getPlayerChallengeService();
    expect(playerChallengeService).not.toBeNull();
  });
});

describe('Factory tests - PlayerConnectionService', () => {
  it('Should get player connection service instance', () => {
    const playerConnectionService = getPlayerConnectionService();
    expect(playerConnectionService).not.toBeNull();
  });
});

describe('Factory tests - PlayerMoveService', () => {
  it('Should get player move service instance', () => {
    const playerMoveService = getPlayerMoveService();
    expect(playerMoveService).not.toBeNull();
  });
});
