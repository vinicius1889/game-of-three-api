import { List } from 'immutable';
import { getDataService } from '../../../src/factory';
import {
  EnumPlayerStatus,
  Player,
  Match,
  EnumMatchStatus,
} from '../../../src/models';
import { IDataService } from '../../../src/services/dataService/IDataService';

describe('Data Service Tests', () => {
  const dataService: IDataService = getDataService();

  it('Should add a new player online ', () => {
    const player: Player = {
      id: '123456',
      avatar: '',
      status: EnumPlayerStatus.IDLE,
    };
    const added = dataService.getOnlinePlayers().push(player);
    dataService.setOnlinePlayer(added);
    const isIncluded = dataService.getOnlinePlayers().includes(player);
    expect(isIncluded).toBeTruthy();
  });

  it('Should get a new player by ID ', () => {
    const player: Player = {
      id: '123456',
      avatar: '',
      status: EnumPlayerStatus.IDLE,
    };
    const added = dataService.getOnlinePlayers().push(player);
    dataService.setOnlinePlayer(added);
    const playerFound = dataService.getOnlinePlayerById(player.id);
    expect(playerFound).toBeDefined();
  });

  it('Should not be able to find a player with the wrong or unknown id ', () => {
    const player: Player = {
      id: '123456',
      avatar: '',
      status: EnumPlayerStatus.IDLE,
    };
    const added = dataService.getOnlinePlayers().push(player);
    dataService.setOnlinePlayer(added);
    const playerNotFound = dataService.getOnlinePlayerById('WRONG_ID');
    expect(playerNotFound).toBeUndefined();
  });

  it('Should add a new match', () => {
    const matchId = `my-custom-match-id--${new Date().getTime()}`;
    const match: Match = {
      challenger: { id: 'a' },
      challenged: { id: 'b' },
      id: matchId,
      status: EnumMatchStatus.WAITING_PLAYER_ACCEPT,
      moves: List(),
    };

    const added = dataService.getMatches().set(matchId, match);
    dataService.setMatches(added);
    const matchFound = dataService.getMatches().has(matchId);
    expect(matchFound).toBeTruthy();
  });
});
