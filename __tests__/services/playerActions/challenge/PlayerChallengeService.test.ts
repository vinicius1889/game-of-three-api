import { List } from 'immutable';
import {
  getDataService,
  getPlayerChallengeService,
  getPlayerConnectionService,
} from '../../../../src/factory';
import { EnumMatchStatus, Match, PlayerMove } from '../../../../src/models';

describe('Player Challenge test ', () => {
  const challengerService = getPlayerChallengeService();
  const connectionService = getPlayerConnectionService();
  const dataService = getDataService();

  it('should challenge a player to participate to a dispute', () => {
    const challenger = '_Challenger_ID_123';
    const challenged = '_Challenged_ID_456';
    connectionService.enterOnline(challenger, '');
    connectionService.enterOnline(challenged, '');

    const { id: matchId } = challengerService.challenge(challenger, challenged);
    const challenge = dataService.getMatches().get(matchId);

    const challengerPlayer = dataService.getOnlinePlayerById(challenger);
    const challengedPlayer = dataService.getOnlinePlayerById(challenged);

    const expectedState: Match = {
      challenger: challengerPlayer,
      challenged: challengedPlayer,
      id: matchId,
      status: EnumMatchStatus.WAITING_PLAYER_ACCEPT,
      moves: List(),
    };

    expect(matchId).not.toBeNull();
    expect(expectedState).toEqual(challenge);
  });

  it('should accept the challenge', () => {
    const time = new Date().getTime();
    const challenger = `_Challenger_ID_${time}`;
    const challenged = `_Challenged_ID_${time}`;
    connectionService.enterOnline(challenger, '');
    connectionService.enterOnline(challenged, '');

    const { id: matchId } = challengerService.challenge(challenger, challenged);
    challengerService.acceptOrReject(challenged, matchId, true);
    const challenge = dataService.getMatches().get(matchId);

    const { status } = challenge;
    expect(challenge).not.toBeNull();
    expect(status).toBe(EnumMatchStatus.START);
  });

  it('should create the first move to the challenged player ', () => {
    const time = new Date().getTime();
    const challenger = `_Challenger_ID_${time}`;
    const challenged = `_Challenged_ID_${time}`;
    connectionService.enterOnline(challenger, '');
    connectionService.enterOnline(challenged, '');

    const { id: matchId } = challengerService.challenge(challenger, challenged);
    challengerService.acceptOrReject(challenged, matchId, true);
    const challenge = dataService.getMatches().get(matchId);
    const playerMove: PlayerMove = challenge.moves.first();
    const { status } = challenge;
    expect(challenge).not.toBeNull();
    expect(playerMove.finalResultNumber).toBeLessThan(100);
    expect(playerMove.finalResultNumber).toBeGreaterThanOrEqual(10);
    expect(playerMove.player.id).toBe(challenger);
    expect(status).toBe(EnumMatchStatus.START);
  });
});
