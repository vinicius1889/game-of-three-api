import {
  getPlayerConnectionService,
  getDataService,
} from '../../../../src/factory';

describe('Player actions tests - Connections ', () => {
  const dataService = getDataService();
  const connectionService = getPlayerConnectionService();

  it('should register a new player online', () => {
    const id = '123456789';
    connectionService.enterOnline(id, '');
    const search = dataService
      .getOnlinePlayers()
      .filter((item) => item.id === id);
    expect(search.size).toBeGreaterThan(0);
  });

  it('should remove the player from the online items', () => {
    const id = 'A_123456789';
    connectionService.enterOnline(id, '');
    connectionService.leaveGame(id);
    const filtered = dataService
      .getOnlinePlayers()
      .filter((item) => item.id == id);
    expect(filtered.size).toBe(0);
  });
});
