import {
  getPlayerMoveService,
  getPlayerChallengeService,
  getPlayerConnectionService,
} from '../../../../src/factory';

import {
  EnumMatchStatus,
  EnumMoveResultIterator,
  EnumMoveSelection,
  Match,
  PlayerMove,
} from '../../../../src/models';

describe('Player Move Service test ', () => {
  const playerMoveService = getPlayerMoveService();
  const playerChallengeService = getPlayerChallengeService();
  const playerConnectionService = getPlayerConnectionService();

  let challenger: string;
  let challenged: string;
  let match: Match;

  /**
   * Setup the player move challenge and accept it before all tests
   */
  beforeAll(() => {
    const time = new Date().getTime();
    challenger = `_player_move_service_challenger_${time}`;
    challenged = `_player_move_service_challenged_${time}`;
    // enter online to register the players
    playerConnectionService.enterOnline(challenger, '');
    playerConnectionService.enterOnline(challenged, '');
    // create a new challenge
    match = playerChallengeService.challenge(challenger, challenged);
    playerChallengeService.acceptOrReject(challenged, match.id, true);
  });

  it('it should send a new move and be able to the next one', () => {
    const playerMove = {
      originalNumber: 30,
      moveSelection: EnumMoveSelection.ZERO,
      player: {
        id: challenger,
      },
    } as PlayerMove;
    const result = playerMoveService.playerTurn(match.id, playerMove);
    const { status } = result.moves.last();
    const { status: matchStatus } = result;
    expect(EnumMoveResultIterator.NEXT).toBe(status);
    expect(EnumMatchStatus.PLAYING).toBe(matchStatus);
  });

  it('it should send a new move and finish the game', () => {
    const playerMove = {
      originalNumber: 3,
      moveSelection: EnumMoveSelection.ZERO,
      player: {
        id: challenged,
      },
    } as PlayerMove;
    const result = playerMoveService.playerTurn(match.id, playerMove);
    const { status, finalResultNumber } = result.moves.last();
    const { status: matchStatus } = result;
    expect(EnumMoveResultIterator.DONE).toEqual(status);
    expect(EnumMatchStatus.FINISHED).toEqual(matchStatus);
    expect(finalResultNumber).toEqual(1);
  });
});
