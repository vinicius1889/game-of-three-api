const fs = require('fs');
const path = require('path');

const file = path.resolve(__dirname, 'players.json');

export const createFileAfterOnline = (id: string) => {
  const content = JSON.parse(fs.readFileSync(file));
  const online = content['online'].slice(-1).concat(id);
  content['online'] = online;
  fs.writeFileSync(file, JSON.stringify(content));
};

export const getIdToChallenge = (myId: string): string => {
  const content = JSON.parse(fs.readFileSync(file));
  return content.online.find((id) => id !== myId);
};
