const io = require('socket.io-client');

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
});

import { createFileAfterOnline, getIdToChallenge } from './PlayingUtils';
import { EnumMatchStatus } from '../../src/models';

/**
 * *****  Short Disclaimer
 *
 * This Demo is a really simple implementation
 *
 *
 * The code here is much more NODE than Typescript properly
 *
 * I didn't spend so much time writing this demo, the proper effor was
 * create the services based on events
 *
 * I hope you like it
 *
 *
 *
 */

/**
 * New connection to our server
 *
 */
const client = io('http://localhost:8888');

/**
 * For an automatic demo, we check if this process is related to the challenged player
 * if it's true, some automatic decisions will be taking in cosideration
 *
 * It's just to make the demo easier and faster
 *
 */
const isListener = process.argv.slice(2).includes('challengedProcess');

/**
 * On connect event listener
 *
 * After the player be online, we will create a new file with the last playersIds
 * It's important to generate default options
 *
 */
client.on('connect', () => {
  console.log('\n\nConnected to the server \n\n');
  createFileAfterOnline(client.id);
});

/**
 * When a new challenge starts, we will receive a new event  related to that
 * We called the start of the game as Challenge.
 *
 * The idea is that you can challenge and be challenged as well accept or deny
 *
 */
client.on(`new_challenge`, (match) => {
  const {
    challenger: { id: challengerId },
  } = match;
  console.log('\nYou have been challenged by ', challengerId);
  answerTheChallenge('', match);
});

/**
 * Answer the challenge emiting the event of challenge accepted
 *
 * @param answer
 * @param match
 */
const answerTheChallenge = (answer: string, match: any) => {
  const { id: matchId } = match;
  const { id: challenged } = client;
  const accepted =
    answer == '' || answer.toString().toUpperCase() == 'Y' ? true : false;
  client.emit('challenge_accepted', { challenged, matchId, accepted });
};

/**
 * Greeting the user and starting the game
 *
 * if he is a "listener" the option to challenge will be not displayed
 * Just to show this demo
 *
 */
client.on('connectResponse', async (id) => {
  console.log('********************');
  console.log('Welcome to our game');
  console.log('Your ID ', id);

  !isListener &&
    readline.question('Do you want to challenge someone? [y,n]', (answer) => {
      challengingPlayer(answer);
    });
});

/**
 * Challenging the player
 *
 * For this demo we look for another player ID
 * It's easier to demo it now
 *
 * For future uses, we have to ask to the user for an ID to challenge
 *
 * @param answer
 */
const challengingPlayer = (answer: string) => {
  const { id } = client;
  if (answer == '' || answer.toString().toUpperCase() == 'Y') {
    const idToChallenge = getIdToChallenge(id);
    console.log(`Challenging the ${idToChallenge}`);
    client.emit('challenge', {
      challenger: id,
      challenged: idToChallenge,
    });
  }
};

/**
 * New game event listener
 * when the game is going to start: It means, all the players accepted the challenge
 * we receive this event to setup all the game
 *
 */
client.on('new_game', (match) => {
  console.log('The match is going to start');
  addMoveEventListener();
  showLastAnswer(match);
});

/**
 * Get the label of who did the move
 *
 * @param move
 */
export const getPlayerName = (move): string => {
  const {
    player: { id },
  } = move;
  return id == client.id ? 'YOU - ' : `(${id}) - `;
};

/**
 * Function that will display the last move properly
 *
 *
 * @param move
 */
const infoFormat = (move): string => {
  const { finalResultNumber } = move;
  return `\n${getPlayerName(move)} played: ${finalResultNumber}`;
};

/**
 * Function that will display the message of who win
 *
 * @param move
 */
const finishingMatchFormat = (move): string => {
  return `\n ****** ${getPlayerName(move)} WIN!!!! ****** \n`;
};

/**
 * Function that show the last answer line
 *
 * @param match
 */
const showLastAnswer = (match) => {
  const lastMove = match.moves.slice(-1)[0];
  const isMyTurn = lastMove.player.id != client.id;
  const info = infoFormat(lastMove);
  console.log(info);
  const isPlaying = validateIfStillInPlace(match);

  if (isMyTurn && isPlaying) {
    readline.question(
      `Please, choose one of the possible operations [-1,0,1]:\n`,
      (answer) => {
        sendAnswer(answer, match);
      },
    );
  }
};

/**
 * Validate if the game still working, if not, it will display a message congratulating
 *
 * @param match
 */
const validateIfStillInPlace = (match): boolean => {
  const isPlaying = match.status != EnumMatchStatus.FINISHED;
  if (!isPlaying) {
    const lastMove = match.moves.slice(-1)[0];
    const message = finishingMatchFormat(lastMove);
    console.log(message);
  }

  return isPlaying;
};

/**
 * Send the answer to the server, the final result number will only be filled after the server calculation
 *
 * @param answer
 * @param match
 */
export const sendAnswer = (answer, match) => {
  const { id: matchId } = match;
  const lastMove = match.moves.slice(-1)[0];
  const { finalResultNumber } = lastMove;

  const intAnswer = parseInt(answer);
  const operation = intAnswer < 0 ? 'MINUS' : intAnswer == 0 ? 'ZERO' : 'PLUS';

  const move = {
    matchId,
    originalNumber: finalResultNumber,
    playerId: client.id,
    operation,
  };
  client.emit('srv_game_move', move);
};

/**
 * For Each turn (move) this function will be triggered
 *
 */
export const addMoveEventListener = () => {
  client.on('usr_game_move', (move) => {
    showLastAnswer(move);
  });
};

/**
 * If you disconnect, we will display the message
 */
client.on('disconnect', () => {
  console.log('>> You lost the connection, please, check the server. <<');
});

console.log('\n\n ****** Playing the game. ****** ');
