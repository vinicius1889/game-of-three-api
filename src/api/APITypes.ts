export type ChallengeRequest = {
  challenger: string;
  challenged: string;
};
export type ChallengedAnswerRequest = {
  challenged: string;
  matchId: string;
  accepted: boolean;
};

export type MoveRequest = {
  matchId: string;
  playerId: string;
  originalNumber: number;
  operation: string;
};
