import { MoveRequest } from './APITypes';
import { EnumMoveSelection, PlayerMove } from '../models';

/**
 * Adapt the move request object to a Player move object
 *
 * @param moveRequest
 */
export const adaptMoveRequestToPlayerMove = (
  moveRequest: MoveRequest,
): PlayerMove =>
  ({
    player: { id: moveRequest.playerId },
    originalNumber: moveRequest.originalNumber,
    moveSelection: EnumMoveSelection[moveRequest.operation.toUpperCase()],
  } as PlayerMove);
