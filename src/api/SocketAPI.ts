import {
  getPlayerConnectionService,
  getPlayerChallengeService,
  getPlayerMoveService,
} from '../factory';

import {
  MoveRequest,
  ChallengeRequest,
  ChallengedAnswerRequest,
} from './APITypes';
import { adaptMoveRequestToPlayerMove } from './Adapter';
import {
  notifyChallengedPlayer,
  notifyChallengeAnswer,
  notifyPlayersAfterMove,
  greetingPlayer,
} from './SocketSender';

/**
 * On connection implementation API
 * @param socket
 */
export const onSocketConnectionEvent = (socket: any): any => {
  const { id, avatar } = socket;
  getPlayerConnectionService().enterOnline(id, avatar);
  greetingPlayer(id, socket);
  createOnPlayerMoveEventListener(socket);
  return socket;
};

/**
 * On new Game request has been sent
 * @param socket
 */
export const onSocketNewGameRequest = (socket: any): any => {
  socket.on('challenge', (challenge: ChallengeRequest) => {
    const { challenger, challenged } = challenge;
    const match = getPlayerChallengeService().challenge(challenger, challenged);
    notifyChallengedPlayer(match, socket);
  });

  socket.on(
    'challenge_accepted',
    (challengeAccepted: ChallengedAnswerRequest) => {
      const { challenged, matchId, accepted } = challengeAccepted;

      const match = getPlayerChallengeService().acceptOrReject(
        challenged,
        matchId,
        accepted,
      );
      notifyChallengeAnswer(match, socket);
    },
  );

  return socket;
};

/**
 * Event triggered after any player's turn
 *
 * @param socket
 */
export const createOnPlayerMoveEventListener = (socket: any): any => {
  socket.on(`srv_game_move`, (move: MoveRequest) => {
    const { matchId } = move;
    const playerMove = adaptMoveRequestToPlayerMove(move);
    const match = getPlayerMoveService().playerTurn(matchId, playerMove);
    notifyPlayersAfterMove(match, socket);
  });
  return socket;
};

/**
 * If player disconnect, for now, he will not be able to re-connect.
 *
 * @param socket
 */
export const onPlayerDisconnect = (socket: any): any => {
  socket.on('disconnect', () => {
    const { id } = socket;
    getPlayerConnectionService().leaveGame(id);
  });
};
