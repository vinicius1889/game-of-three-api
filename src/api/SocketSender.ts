import { Match, EnumMatchStatus } from '../models';

/**
 * Notify challenged player
 * He can accept or deny, for this test demonstration only acceptance will be cared
 * @param match
 * @param socket
 */
export const notifyChallengedPlayer = (match: Match, socket: any) => {
  const {
    challenged: { id: challengedId },
    status,
  } = match;

  if (status == EnumMatchStatus.WAITING_PLAYER_ACCEPT) {
    socket.to(challengedId).emit(`new_challenge`, match);
  }
};

/**
 * Notify the players about the answer
 *
 * @param match
 * @param socket
 */
export const notifyChallengeAnswer = (match: Match, socket: any) => {
  const {
    status,
    challenger: { id: challengerId },
  } = match;
  if (status == EnumMatchStatus.START) {
    const matchToSend = { ...match, moves: match.moves.toJSON() };
    //notify the challenger the answer
    socket.to(challengerId).emit(`new_game`, matchToSend);
    //send to the current "room" my own answer to start the game in both sides
    socket.emit(`new_game`, matchToSend);
  }
};

/**
 * Send an event to all users regarding the last move
 *
 * @param match
 * @param socket
 */
export const notifyPlayersAfterMove = (match: Match, socket: any) => {
  const {
    challenged: { id: challengedId },
    challenger: { id: challengerId },
  } = match;

  [challengedId, challengerId].map((room) => {
    if (room === socket.id) {
      socket.emit('usr_game_move', match);
    } else {
      socket.to(room).emit('usr_game_move', match);
    }
  });
};

/**
 * Greeting the player (id) emiting the event
 *
 * @param id
 * @param socket
 */
export const greetingPlayer = (id: string, socket: any) => {
  socket.emit('connectResponse', id);
};
