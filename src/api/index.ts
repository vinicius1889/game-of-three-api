import {
  onSocketConnectionEvent,
  onSocketNewGameRequest,
  onPlayerDisconnect,
} from './SocketAPI';
import { compose } from 'ramda';

/**
 * Set all events to a Socket IO server
 *
 * @param server
 */
export const handleSocketAPI = (server: any): void => {
  /**
   * On Connection event handle
   * Triggered only once. After the user connect, the event will be sent
   */
  server.on('connection', (socket) => {
    const composedEvents = compose(
      onPlayerDisconnect,
      onSocketNewGameRequest,
      onSocketConnectionEvent,
    );
    composedEvents(socket);
  });
};
