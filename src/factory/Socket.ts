const io = require('socket.io');

/**
 * Create a socket server with default values
 *
 */
export const createSocketServer = (): any => io({});
