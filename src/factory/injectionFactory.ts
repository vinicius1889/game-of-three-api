import container from '../services/IoC/inversify.config';
import { SERVICE_TYPES } from '../services/IoC/serviceTypes';
import { IDataService } from '../services/dataService/IDataService';
import {
  IPlayerChallengeService,
  IPlayerConnectionService,
  IPlayerMoveService,
} from '../services/playerActions';

/**
 * Return the IDataService Standalone
 *
 */
export const getDataService = (): IDataService =>
  container.get<IDataService>(SERVICE_TYPES.IDataService);

/**
 * Return the IPlayerConnectionService
 */
export const getPlayerConnectionService = (): IPlayerConnectionService =>
  container.get<IPlayerConnectionService>(
    SERVICE_TYPES.IPlayerConnectionService,
  );

/**
 * Challenger service
 */
export const getPlayerChallengeService = (): IPlayerChallengeService =>
  container.get<IPlayerChallengeService>(SERVICE_TYPES.IPlayerChallengeService);

/**
 * Return a new Player Move Service instance
 */
export const getPlayerMoveService = (): IPlayerMoveService =>
  container.get<IPlayerMoveService>(SERVICE_TYPES.IPlayerMoveService);
