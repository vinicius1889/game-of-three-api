import { List } from 'immutable';
import { PlayerMove } from './PlayerMove';
import { Player } from './Player';

export type Match = {
  id: string;
  challenger: Player;
  challenged: Player;
  moves?: List<PlayerMove>;
  status: EnumMatchStatus;
};

export enum EnumMatchStatus {
  WAITING_PLAYER_ACCEPT,
  PLAYING,
  REJECTED,
  START,
  FINISHED,
}
