export type Player = {
  id: string;
  avatar?: string;
  status?: EnumPlayerStatus;
};

export enum EnumPlayerStatus {
  PLAYING,
  IDLE,
}
