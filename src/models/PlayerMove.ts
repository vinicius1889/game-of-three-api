import { Player } from './Player';

export enum EnumMoveSelection {
  MINUS,
  ZERO,
  PLUS,
}

export enum EnumMoveResultIterator {
  NEXT,
  DONE,
}

export type PlayerMove = {
  player: Player;
  originalNumber: number;
  finalResultNumber: number;
  status: EnumMoveResultIterator;
  moveSelection: EnumMoveSelection;
};
