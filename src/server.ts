import { createSocketServer } from './factory/Socket';
import { handleSocketAPI } from './api';

const server = createSocketServer();

handleSocketAPI(server);

server.listen(process.env.PORT ?? 8888);
