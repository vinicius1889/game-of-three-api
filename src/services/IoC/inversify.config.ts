import 'reflect-metadata';

import { Container } from 'inversify';
import { DataService } from '../dataService/DataService';
import { IDataService } from '../dataService/IDataService';
import {
  PlayerConnectionService,
  IPlayerConnectionService,
  PlayerChallengeService,
  IPlayerChallengeService,
  PlayerMoveService,
  IPlayerMoveService,
} from '../playerActions';

import { SERVICE_TYPES } from './serviceTypes';

let container = new Container();

container
  .bind<IDataService>(SERVICE_TYPES.IDataService)
  .to(DataService)
  .inSingletonScope();

container
  .bind<IPlayerConnectionService>(SERVICE_TYPES.IPlayerConnectionService)
  .to(PlayerConnectionService);

container
  .bind<IPlayerChallengeService>(SERVICE_TYPES.IPlayerChallengeService)
  .to(PlayerChallengeService);

container
  .bind<IPlayerMoveService>(SERVICE_TYPES.IPlayerMoveService)
  .to(PlayerMoveService);

export default container;
