export const SERVICE_TYPES = {
  IPlayerConnectionService: Symbol.for('IPlayerConnectionService'),
  IPlayerChallengeService: Symbol.for('IPlayerChallengeService'),
  IDataService: Symbol.for('IDataService'),
  IPlayerMoveService: Symbol.for('IPlayerMoveService'),
};
