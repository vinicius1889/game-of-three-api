import { injectable } from 'inversify';
import { List, Map } from 'immutable';
import { Player, Match } from '../../models';
import { IDataService } from './IDataService';

@injectable()
export class DataService implements IDataService {
  private onlinePlayers: List<Player> = List();

  private matches: Map<string, Match> = Map();

  getOnlinePlayers(): List<Player> {
    return this.onlinePlayers;
  }

  setOnlinePlayer(players: List<Player>): void {
    this.onlinePlayers = players;
  }
  getOnlinePlayerById(id: string): Player {
    return this.onlinePlayers.find((player) => player.id == id);
  }

  getMatches(): Map<string, Match> {
    return this.matches;
  }

  setMatches(matches: Map<string, Match>): void {
    this.matches = matches;
  }
}
