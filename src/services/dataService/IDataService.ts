import { List, Map } from 'immutable';
import { Player, Match } from '../../models';
export interface IDataService {
  getOnlinePlayers(): List<Player>;
  setOnlinePlayer(players: List<Player>): void;
  getOnlinePlayerById(id: string): Player;
  getMatches(): Map<string, Match>;

  setMatches(matches: Map<string, Match>): void;
}
