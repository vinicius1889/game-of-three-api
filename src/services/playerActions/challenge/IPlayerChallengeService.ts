import { Match } from '../../../models';

export interface IPlayerChallengeService {
  challenge(challenger: string, challenged: string): Match;
  acceptOrReject(challenged: string, matchId: string, accepted: boolean): Match;
}
