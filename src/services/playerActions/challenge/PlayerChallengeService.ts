import { inject, injectable } from 'inversify';
import { List } from 'immutable';
import { v4 as uuid } from 'uuid';

import { IPlayerChallengeService } from './IPlayerChallengeService';
import { IDataService } from '../../dataService/IDataService';
import { SERVICE_TYPES } from '../../IoC/serviceTypes';
import { Match, EnumMatchStatus, PlayerMove } from '../../../models';
import { validatePlayersAreAvailable } from './PlayerChallengeValidationService';
import { createFirstMove } from '../game/PlayerMoveUtils';

@injectable()
export class PlayerChallengeService implements IPlayerChallengeService {
  constructor(
    @inject(SERVICE_TYPES.IDataService) private dataService: IDataService,
  ) {}

  /**
   * Challenge a player and return the challenge ID
   *
   * @param challenger
   * @param challenged
   */
  challenge(challenger: string, challenged: string): Match {
    const id: string = uuid();

    const challengerPlayer = this.dataService.getOnlinePlayerById(challenger);
    const challengedPlayer = this.dataService.getOnlinePlayerById(challenged);

    const match: Match = {
      id,
      challenger: challengerPlayer,
      challenged: challengedPlayer,
      status: EnumMatchStatus.WAITING_PLAYER_ACCEPT,
      moves: List(),
    };

    //TODO: add some validations here
    const matches = this.dataService.getMatches();
    const updated = matches.set(id, match);
    this.dataService.setMatches(updated);
    return match;
  }

  /**
   * Accept or reject the challenge
   * @param challenged
   * @param matchId
   */
  acceptOrReject(
    challenged: string,
    matchId: string,
    accepted: boolean,
  ): Match {
    const matches = this.dataService.getMatches();
    const onlineUsers = this.dataService.getOnlinePlayers();
    const match = matches.get(matchId);

    validatePlayersAreAvailable(match, onlineUsers);
    //TODO: validate if it was accepted or not
    match.status = accepted ? EnumMatchStatus.START : EnumMatchStatus.REJECTED;
    match.moves = List<PlayerMove>();
    match.moves = match.moves.push(createFirstMove(match));
    if (match.challenged.id !== challenged) {
      throw new Error('Challenged id is not correct');
    }
    const updated = matches.set(matchId, match);
    this.dataService.setMatches(updated);
    return match;
  }
}
