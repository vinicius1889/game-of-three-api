import { EnumPlayerStatus, Match, Player } from '../../../models';
import { List } from 'immutable';

export const validatePlayersAreAvailable = (
  match: Match,
  onlinePlayers: List<Player>,
) => {
  const {
    challenged: { id: challengedId },
    challenger: { id: challengerId },
  } = match;
  const isBusy =
    onlinePlayers
      .filter((player) => [challengerId, challengedId].includes(player.id))
      .filter((player) => player.status == EnumPlayerStatus.PLAYING).size > 0;
  if (isBusy) {
    throw Error(
      'The match cannot start, one or more players are playing at this point in the time.',
    );
  }
};
