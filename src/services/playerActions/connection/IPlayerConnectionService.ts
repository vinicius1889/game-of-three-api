import IPlayerLeaveService from './IPlayerLeaveService';
import IPlayerSetupService from './IPlayerSetupService';

export interface IPlayerConnectionService
  extends IPlayerLeaveService,
    IPlayerSetupService {}
