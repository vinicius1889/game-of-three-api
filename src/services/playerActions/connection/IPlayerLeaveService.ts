export default interface IPlayerLeaveService {
  /**
   * Method resposable to execute the actions when the user leave the game.
   *
   * @param id
   */
  leaveGame(id: string);
}
