export default interface IPlayerSetupService {
  /**
   * Method needed to show the user as an available player to be challenged or init one.
   *
   * @param id
   */
  enterOnline(id: string, avatar: string);
}
