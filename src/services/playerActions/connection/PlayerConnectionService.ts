import { inject, injectable } from 'inversify';
import { List } from 'immutable';
import { IDataService } from '../../dataService/IDataService';
import { IPlayerConnectionService } from './IPlayerConnectionService';

import { SERVICE_TYPES } from '../../IoC/serviceTypes';

import { EnumPlayerStatus, Player } from '../../../models';

@injectable()
export class PlayerConnectionService implements IPlayerConnectionService {
  constructor(
    @inject(SERVICE_TYPES.IDataService) private dataService: IDataService,
  ) {}

  leaveGame(id: string) {
    const players: List<Player> = this.dataService
      .getOnlinePlayers()
      .filterNot((_item) => _item.id === id);
    this.dataService.setOnlinePlayer(players);
  }
  enterOnline(id: string, avatar: string): void {
    const player: Player = { id, avatar, status: EnumPlayerStatus.IDLE };
    const players = this.dataService.getOnlinePlayers().push(player);
    this.dataService.setOnlinePlayer(players);
  }
}
