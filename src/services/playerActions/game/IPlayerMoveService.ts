import { Match, PlayerMove } from '../../../models';
export interface IPlayerMoveService {
  playerTurn(matchId: string, playerMove: PlayerMove): Match;
}
