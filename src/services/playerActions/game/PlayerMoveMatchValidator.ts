import {
  EnumMatchStatus,
  EnumMoveResultIterator,
  Match,
} from '../../../models';

export const validateMatchStatus = (match: Match): Match => {
  const { status: lastMoveStatus } = match.moves.last();
  const updatedMatch = { ...match };
  updatedMatch.status =
    lastMoveStatus == EnumMoveResultIterator.NEXT
      ? EnumMatchStatus.PLAYING
      : EnumMatchStatus.FINISHED;
  return updatedMatch;
};
