import { inject, injectable } from 'inversify';

import { Match, PlayerMove } from '../../../models';
import { IPlayerMoveService } from './IPlayerMoveService';
import { IDataService } from '../../dataService/IDataService';

import { SERVICE_TYPES } from '../../IoC/serviceTypes';
import { checkPlayerMoveWin } from './PlayerMoveValidatorService';
import { validateMatchStatus } from './PlayerMoveMatchValidator';

@injectable()
export class PlayerMoveService implements IPlayerMoveService {
  constructor(
    @inject(SERVICE_TYPES.IDataService) private dataService: IDataService,
  ) {}

  playerTurn(matchId: string, playerMove: PlayerMove): Match {
    const updatedPlayedMove = checkPlayerMoveWin(playerMove);
    const match = this.dataService.getMatches().get(matchId);
    match.moves = match.moves.push(updatedPlayedMove);

    const updatedMatch = validateMatchStatus(match);
    this.dataService.getMatches().set(matchId, updatedMatch);
    return updatedMatch;
  }
}
