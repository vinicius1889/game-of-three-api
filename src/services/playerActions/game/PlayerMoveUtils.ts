import {
  Match,
  PlayerMove,
  EnumMoveResultIterator,
  EnumMoveSelection,
} from '../../../models';

export const createFirstMove = (match: Match): PlayerMove => {
  const randomNumber = Math.random() * (99 - 10) + 10;
  const originalNumber = Math.ceil(randomNumber);
  const updatedMatch = { ...match };
  const { challenger: player } = updatedMatch;
  const firstMoveToChallengedPlayer = {
    player,
    originalNumber,
    finalResultNumber: originalNumber,
    status: EnumMoveResultIterator.NEXT,
    moveSelection: EnumMoveSelection.ZERO,
  } as PlayerMove;
  return firstMoveToChallengedPlayer;
};
