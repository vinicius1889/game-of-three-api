import {
  EnumMoveResultIterator,
  EnumMoveSelection,
  PlayerMove,
} from '../../../models';

/**
 * Validate if the game still in place
 * @param move
 */
export const checkPlayerMoveWin = (move: PlayerMove): PlayerMove => {
  const tempMove = { ...move };
  const numberResult = playerMoveCalculation(move);
  tempMove.finalResultNumber = numberResult;
  tempMove.status =
    numberResult == 1
      ? EnumMoveResultIterator.DONE
      : EnumMoveResultIterator.NEXT;
  return tempMove;
};

/**
 * Calculate the move
 * @param move
 */
export const playerMoveCalculation = (move: PlayerMove): number => {
  const DIVIDER = 3;
  return (
    (move.originalNumber + getNumberFromMoveSelection(move.moveSelection)) /
    DIVIDER
  );
};

/**
 * Translate the enum move selection to a proper number, the default value will be 3
 * @param enumMoveSelection
 * @param base
 */
export const getNumberFromMoveSelection = (
  enumMoveSelection: EnumMoveSelection,
  base: number = 1,
): number => {
  switch (enumMoveSelection) {
    case EnumMoveSelection.MINUS:
      return base * -1;
    case EnumMoveSelection.PLUS:
      return base;
    default:
      return 0;
  }
};
